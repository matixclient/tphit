package com.PintsizedSix40.plugin;

import de.paxii.clarinet.Wrapper;
import de.paxii.clarinet.event.EventHandler;
import de.paxii.clarinet.event.events.player.PlayerAttackEntityEvent;
import de.paxii.clarinet.event.events.player.UpdatePlayerMoveStateEvent;
import de.paxii.clarinet.module.Module;
import de.paxii.clarinet.module.ModuleCategory;

public class ModuleTpHit extends Module {

  private boolean didPlayerAttack;

  public ModuleTpHit() {
    super("TpHit", ModuleCategory.COMBAT);

    this.setVersion("1.0");
    this.setBuildVersion(18200);
    this.setDescription("Makes you go back a block when hitting someone. Also makes you go upwards if you jump and hit.");
    this.setRegistered(true);
  }

  @EventHandler
  public void playerAttackEntityEvent(PlayerAttackEntityEvent attack) {
    Wrapper.getPlayer().noClip = true;
    this.didPlayerAttack = true;
  }

  @EventHandler
  public void onUpdateMoveState(UpdatePlayerMoveStateEvent updateEvent) {
    if (this.didPlayerAttack) {
      updateEvent.setMoveForward(3);
      updateEvent.setMoveForward(-3);
      if (Wrapper.getPlayer().onGround) {
        Wrapper.getPlayer().jump();
        Wrapper.getPlayer().setJumping(true);
      }
      updateEvent.setMoveStrafe(-6);
      this.didPlayerAttack = false;
    }
  }
}
